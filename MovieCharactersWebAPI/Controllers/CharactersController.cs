﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersWebAPI.Models;
using MovieCharactersWebAPI.Models.DTO.Character;
using MovieCharactersWebAPI.Services;

namespace MovieCharactersWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICharacterService _characterService;

        public CharactersController(IMapper mapper,  ICharacterService characterService)
        {
            _characterService = characterService;
            _mapper = mapper;
        }
        /// <summary>
        /// Returns all characters.
        /// </summary>
        /// <returns></returns>
        // GET: api/Characters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            var characters = await _characterService.GetAllCharactersAsync();
            return _mapper.Map<List<CharacterReadDTO>>(characters);
        }
        /// <summary>
        /// Returns a specific character.
        /// </summary>
        /// <param name="id">Character Id.</param>
        /// <returns></returns>
        // GET: api/Characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            Character character = await _characterService.GetSpecificCharacterAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }
        /// <summary>
        /// Updates a specific character.
        /// </summary>
        /// <param name="id">Id of a character.</param>
        /// <param name="dtoCharacter">Character DTO object.</param>
        /// <returns></returns>
        // PUT: api/Characters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO dtoCharacter)
        {
            if (id != dtoCharacter.Id)
            {
                return BadRequest();
            }

            if (!_characterService.CharacterExists(id)) {
                return NotFound();
            }

            Character character = _mapper.Map<Character>(dtoCharacter);
            await _characterService.UpdateCharacterAsync(character);

            return NoContent();
        }
        /// <summary>
        /// Creates a character.
        /// </summary>
        /// <param name="dtoCharacter">Character DTO object.</param>
        /// <returns></returns>
        // POST: api/Characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CharacterCreateDTO dtoCharacter)
        {
            Character newCharacter = _mapper.Map<Character>(dtoCharacter);

            newCharacter = await _characterService.AddCharacterAsync(newCharacter);
           
            return CreatedAtAction("GetCharacter", new { id = newCharacter.Id }, newCharacter);
        }
        /// <summary>
        /// Delets a character.
        /// </summary>
        /// <param name="id">Id of a character.</param>
        /// <returns></returns>
        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }

            await _characterService.DeleteCharacterAsync(id);

            return NoContent();
        }
    }
}
