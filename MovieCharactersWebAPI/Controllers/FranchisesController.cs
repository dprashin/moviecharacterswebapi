﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersWebAPI.Models;
using MovieCharactersWebAPI.Models.DTO.Character;
using MovieCharactersWebAPI.Models.DTO.Franchise;
using MovieCharactersWebAPI.Models.DTO.Movie;
using MovieCharactersWebAPI.Services;

namespace MovieCharactersWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;

        public FranchisesController(IMapper mapper, IFranchiseService franchiseService)
        {
            _mapper = mapper;
            _franchiseService = franchiseService;
        }

        /// <summary>
        /// Returns all franchises
        /// </summary>
        /// <returns></returns>
        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            var characters = await _franchiseService.GetAllFranchiseAsync();
            return _mapper.Map<List<FranchiseReadDTO>>(characters);
        }

        /// <summary>
        /// Returns a franchise with a specific id
        /// </summary>
        /// <param name="id">If of franchise</param>
        /// <returns></returns>
        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            Franchise franchise = await _franchiseService.GetSpecificFranchiseAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Updates a franchise with specific Id
        /// </summary>
        /// <param name="id">Id of franchise</param>
        /// <param name="dtoFranchise">Franchise DTO object</param>
        /// <returns></returns>
        // PUT: api/Franchises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO dtoFranchise)
        {
            if (id != dtoFranchise.Id) {
                return BadRequest();
            }

            if (!_franchiseService.FranchiseExists(id)) {
                return NotFound();
            }

            Franchise franchise = _mapper.Map<Franchise>(dtoFranchise);
            await _franchiseService.UpdatFranchiseAsync(franchise);

            return NoContent();
        }

        /// <summary>
        /// Assigns movies to a specific franchise
        /// </summary>
        /// <param name="id">Id of franchise</param>
        /// <param name="movieIds">Ids of movies</param>
        /// <returns></returns>
        // PUT: api/Franchises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}/movieIds")]
        public async Task<IActionResult> UpdateMoviesInFranchies(int id, int[] movieIds)
        {
            if (!_franchiseService.FranchiseExists(id)) {
                return NotFound();
            }

            try {
                await _franchiseService.UpdateMoviesInFranchiseAsync(id, movieIds);
            } catch (KeyNotFoundException) {

                return BadRequest("Invalid movie");
            }
            return NoContent();
        }

        /// <summary>
        /// Adds a new franchise
        /// </summary>
        /// <param name="dtoFranchise">Franchise DTO object</param>
        /// <returns></returns>
        // POST: api/Franchises
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO dtoFranchise)
        {
            Franchise newFranchise = _mapper.Map<Franchise>(dtoFranchise);

            newFranchise = await _franchiseService.AddFranchiseAsync(newFranchise);

            return CreatedAtAction("GetCharacter", new { id = newFranchise.Id }, newFranchise);
        }

        /// <summary>
        /// Deletes a franchise
        /// </summary>
        /// <param name="id">Id of franchise</param>
        /// <returns></returns>
        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id)) {
                return NotFound();
            }

            await _franchiseService.DeleteFranchiseAsync(id);

            return NoContent();
        }

        /// <summary>
        /// Returns movies belonging to specific franchise
        /// </summary>
        /// <param name="id">Id of franchise</param>
        /// <returns></returns>
        // GET: api/Franchises/5/movies
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetFranchiseMovies(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<List<MovieReadDTO>>(await _franchiseService.GetMoviesInFranchise(id));
        }

        /// <summary>
        /// Returns characters belonging to specific franchise
        /// </summary>
        /// <param name="id">Id of franchise</param>
        /// <returns></returns>
        // GET: api/Franchises/5/characters
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetFranchiseCharacters(int id)
        {

            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<List<CharacterReadDTO>>(await _franchiseService.GetCharactersInFranchise(id));
        }
    }
}
