﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersWebAPI.Models;
using MovieCharactersWebAPI.Models.DTO.Character;
using MovieCharactersWebAPI.Models.DTO.Movie;
using MovieCharactersWebAPI.Services;

namespace MovieCharactersWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        public MoviesController(IMapper mapper, IMovieService movieService)
        {
            _movieService = movieService;
            _mapper = mapper;
        }

        /// <summary>
        /// Return all movies
        /// </summary>
        /// <returns></returns>
        // GET: api/Movies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            var movies = await _movieService.GetAllMoviesAsync();
            return _mapper.Map<List<MovieReadDTO>>(movies);
        }

        /// <summary>
        /// Returns a movie with specific id
        /// </summary>
        /// <param name="id">Id of movie</param>
        /// <returns></returns>
        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            Movie movie = await _movieService.GetSpecificMovieAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Updates movie with specific id
        /// </summary>
        /// <param name="id">Id of movie</param>
        /// <param name="dtoMovie">Movie DTO object</param>
        /// <returns></returns>
        // PUT: api/Movies/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO dtoMovie)
        {
            if (id != dtoMovie.Id)
            {
                return BadRequest();
            }

            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            Movie movie = _mapper.Map<Movie>(dtoMovie);
            await _movieService.UpdateMovieAsync(movie);

            return NoContent();
        }

        /// <summary>
        /// Assigns characters to a movie
        /// </summary>
        /// <param name="id">Id of movie</param>
        /// <param name="characterIds">List of character ids</param>
        /// <returns></returns>
        // PUT: api/Movies/5/characterIds=1&characterIds=2
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}/characterIds")]
        public async Task<IActionResult> UpdateCharactersInMovie(int id, int[] characterIds)
        {

            {
                if (!_movieService.MovieExists(id))
                {
                    return NotFound();
                }

                try
                {
                    await _movieService.UpdateCharactersInMovieAsync(id, characterIds);
                }
                catch (KeyNotFoundException)
                {
                    return BadRequest("Invalid character.");
                }
                return NoContent();
            }
        }

        /// <summary>
        /// Creates a new movie
        /// </summary>
        /// <param name="dtoMovie">Movie DTO object</param>
        /// <returns></returns>
        // POST: api/Movies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieCreateDTO dtoMovie)
        {
            Movie newMovie = _mapper.Map<Movie>(dtoMovie);

            newMovie = await _movieService.AddMovieAsync(newMovie);

            return CreatedAtAction("GetCharacter", new { id = newMovie.Id }, newMovie);
        }

        /// <summary>
        /// Deletes a movie
        /// </summary>
        /// <param name="id">Id of movie</param>
        /// <returns></returns>
        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            await _movieService.DeleteMovieAsync(id);

            return NoContent();
        }

        /// <summary>
        /// Returns characters of a specific movie
        /// </summary>
        /// <param name="id">Id of movie</param>
        /// <returns></returns>
        // GET: api/Movies/5/characters
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetMovieCharacters(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<List<CharacterReadDTO>>(await _movieService.GetMovieCharactersAsync(id));

        }
    }
}
