﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersWebAPI.Models
{
    public class Character
    {
        // Pk
        public int Id { get; set; }
        // Fields
        [Required]
        [MaxLength(50)]
        public string FullName { get; set; }
        [MaxLength(10)]
        public string Alias { get; set; }
        [MaxLength(6)]
        public string Gender { get; set; }
        [MaxLength(200)]
        public string PictureUrl { get; set; }
        // Relationships
        public ICollection<Movie>? Movies { get; set; }
    }
}
