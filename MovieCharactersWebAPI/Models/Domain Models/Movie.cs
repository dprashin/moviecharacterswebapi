﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersWebAPI.Models {
    public class Movie
    {
        // Pk
        public int Id { get; set; }
        // Fields
        [Required]
        [MaxLength(50)]
        public string MovieTitle { get; set; }
        [MaxLength(100)]
        public string Genre { get; set; }
        [Required]
        [Range(1900, 2022)]
        public int ReleaseYear { get; set; }
        [Required]
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(200)]
        public string PictureUrl { get; set; }
        [MaxLength(200)]
        public string TrailerUrl { get; set; }
        // Relationships
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        public ICollection<Character>? Characters { get; set; }
    }
}
