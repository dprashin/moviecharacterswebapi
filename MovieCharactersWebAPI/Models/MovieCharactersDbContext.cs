﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersWebAPI.Models
{
    public class MovieCharactersDbContext : DbContext 
    {
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MovieCharactersDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed data
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 1, Name = "Lord of the Rings", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 2, Name = "The Hobbit", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 3, Name = "Harry Potter", Description = " Lorem Ipsum is simply dummy text of the printing and typesetting industry." });

            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 1, MovieTitle = "The Return of the King", Genre = "Action, Adventure, Drama", ReleaseYear = 2003, Director = "Peter Jackson", PictureUrl = "", TrailerUrl = "", FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 2, MovieTitle = "Harry Potter and the Chamber of Secrets", Genre = "Adventure, Family, Fantasy", ReleaseYear = 2002, Director = "Chris Columbus", PictureUrl = "https://www.imdb.com/title/tt0295297/mediaviewer/rm1029675264/", TrailerUrl = "https://www.youtube.com/watch?v=1bq0qff4iF8", FranchiseId = 3 });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 3, MovieTitle = "An Unexpected Journey", Genre = "Adventure, Fantasy", ReleaseYear = 2012, Director = "Peter Jackson", PictureUrl = "", TrailerUrl = "", FranchiseId = 2 });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 4, MovieTitle = "Harry Potter and the Prisoner of Azkaban", Genre = "Adventure, Family, Fantasy", ReleaseYear = 2004, Director = "Alfonso Cuarón", PictureUrl = "https://www.imdb.com/title/tt0304141/mediaviewer/rm3241184256/", TrailerUrl = "", FranchiseId = 3 });

            modelBuilder.Entity<Character>().HasData(new Character() { Id = 1, FullName = "Gollum", Alias = "Smeagol", Gender = "Male", PictureUrl = ""});
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 2, FullName = "Harry Potter", Alias = "Harry", Gender = "Male", PictureUrl = ""});
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 3, FullName = "Aragorn II Elessar", Alias = "Strider", Gender = "Male", PictureUrl = "" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 4, FullName = "Hermione Granger", Alias = "Hermione", Gender = "Female", PictureUrl = ""});
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 5, FullName = "Bilbo Baggins", Alias = "", Gender = "Male", PictureUrl = "" });


            // Seed m2m coach-certification. Need to define m2m and access linking table
            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharactersId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MoviesId"),
                    je =>
                    {
                        je.HasKey("MoviesId", "CharactersId");
                        je.HasData(
                            new { MoviesId = 1, CharactersId = 1 },
                            new { MoviesId = 2, CharactersId = 2 },
                            new { MoviesId = 2, CharactersId = 4 },
                            new { MoviesId = 3, CharactersId = 5 },
                            new { MoviesId = 3, CharactersId = 1 },
                            new { MoviesId = 4, CharactersId = 2 },
                            new { MoviesId = 4, CharactersId = 4 },
                            new { MoviesId = 1, CharactersId = 5 },
                            new { MoviesId = 1, CharactersId = 3 }
                        );
                    });
        }
    }
}
