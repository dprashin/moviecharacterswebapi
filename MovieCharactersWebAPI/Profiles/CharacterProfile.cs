﻿using AutoMapper;
using MovieCharactersWebAPI.Models;
using MovieCharactersWebAPI.Models.DTO.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersWebAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            // Character <-> CharacterReadDTO
            CreateMap<Character, CharacterReadDTO>()
                .ReverseMap();

            // CharacterCreateDTO <-> Character
            CreateMap<CharacterCreateDTO, Character>()
                .ReverseMap();

            // CharacterEditDTO <-> Character
            CreateMap<CharacterEditDTO, Character>()
                .ReverseMap();
        }
    }
}
