﻿using MovieCharactersWebAPI.Models;
using MovieCharactersWebAPI.Models.DTO.Franchise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;


namespace MovieCharactersWebAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            // Franchise <-> FranchiseReadDTO
            CreateMap<Franchise, FranchiseReadDTO>()
                .ReverseMap();

            // FranchiseCreateDTO <-> Franchise
            CreateMap<FranchiseCreateDTO, Franchise>()
                .ReverseMap();

            // FranchiseEditDTO <-> Franchise
            CreateMap<FranchiseEditDTO, Franchise>()
                .ReverseMap();
        }
    }
}
