﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MovieCharactersWebAPI.Models;
using MovieCharactersWebAPI.Models.DTO.Movie;

namespace MovieCharactersWebAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            // Movie <-> MovieReadDTO
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Franchise, opt => opt
                .MapFrom(c => c.FranchiseId))
                .ReverseMap();

            // MovieCreateDTO <-> Movie
            CreateMap<Movie, MovieCreateDTO>()
                .ForMember(mdto => mdto.Franchise, opt => opt
                .MapFrom(c => c.FranchiseId))
                .ReverseMap();

            // MovieEditDTO <-> Movie
            CreateMap<Movie, MovieEditDTO>()
                .ForMember(mdto => mdto.Franchise, opt => opt
                .MapFrom(c => c.FranchiseId))
                .ReverseMap();
        }
    }
}
