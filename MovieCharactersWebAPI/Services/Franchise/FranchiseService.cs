﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace MovieCharactersWebAPI.Services {
    public class FranchiseService : IFranchiseService {
        private readonly MovieCharactersDbContext _context;

        public FranchiseService(MovieCharactersDbContext context) {
            _context = context;
        }
        public async Task<Franchise> AddFranchiseAsync(Franchise franchise) {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        public async Task DeleteFranchiseAsync(int id) {
            var character = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(character);
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExists(int id) {
            return _context.Characters.Any(character => character.Id == id);
        }

        public async Task<IEnumerable<Franchise>> GetAllFranchiseAsync() {
            return await _context.Franchises.ToListAsync();
        }

        
        public async Task<IEnumerable<Character>> GetCharactersInFranchise(int id) {
            var franchise = await _context.Franchises.FindAsync(id);
            
            if(franchise == null) {
                throw new KeyNotFoundException();
            }

            return await _context.Movies.Where(m => m.FranchiseId == id).SelectMany(m => m.Characters).Distinct().ToListAsync();

        }

        public async Task<IEnumerable<Movie>> GetMoviesInFranchise(int id) {
            var franchise = await _context.Franchises.FindAsync(id);
            if(franchise == null) {
                throw new KeyNotFoundException();
            }

            return await _context.Movies.Where(m => m.FranchiseId == id).ToListAsync();
        }


        public async Task<Franchise> GetSpecificFranchiseAsync(int id) {
            return await _context.Franchises.FindAsync(id);
        }

        public async Task UpdateMoviesInFranchiseAsync(int id, int[] movieIds) {
 
            // first fetch all the movies in the db
            Franchise franchiseToUpdateMovies = await _context.Franchises
                .Include(c => c.Movies)
                .Where(m => m.Id == id)
                .FirstAsync();
            List<Movie> movies = new(franchiseToUpdateMovies.Movies);

            // check if movies with ids specified in movieIds exist in database
            foreach (int movieId in movieIds) {
                Movie movie = await _context.Movies.FindAsync(movieId);
                if (movie == null)
                if (movie == null)
                    throw new KeyNotFoundException();
                movies.Add(movie);

            }
            franchiseToUpdateMovies.Movies = movies;
            await _context.SaveChangesAsync();
        }

        public async Task UpdatFranchiseAsync(Franchise franchise) {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
