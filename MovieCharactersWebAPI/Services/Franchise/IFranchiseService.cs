﻿using MovieCharactersWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersWebAPI.Services {
    public interface IFranchiseService {
        public Task<IEnumerable<Franchise>> GetAllFranchiseAsync();
        public Task<Franchise> GetSpecificFranchiseAsync(int id);
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);
        public Task UpdatFranchiseAsync(Franchise franchise);
        public Task DeleteFranchiseAsync(int id);
        public bool FranchiseExists(int id);
        public Task<IEnumerable<Movie>> GetMoviesInFranchise(int id);
        public Task<IEnumerable<Character>> GetCharactersInFranchise(int id);
        public Task UpdateMoviesInFranchiseAsync(int id, int[] movieIds);
    }
}
