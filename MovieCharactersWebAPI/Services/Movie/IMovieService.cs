﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MovieCharactersWebAPI.Models;

namespace MovieCharactersWebAPI.Services
{
    public interface IMovieService
    {
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        public Task<Movie> GetSpecificMovieAsync(int id);
        public Task<Movie> AddMovieAsync(Movie movie);
        public Task UpdateMovieAsync(Movie movie);
        public Task DeleteMovieAsync(int id);
        public Task UpdateCharactersInMovieAsync(int id, int[] characterIds);
        public Task<IEnumerable<Character>> GetMovieCharactersAsync(int id);
        public bool MovieExists(int id);
    }
}
