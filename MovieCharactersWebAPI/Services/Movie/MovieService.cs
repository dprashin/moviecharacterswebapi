﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersWebAPI.Services
{
    public class MovieService : IMovieService
    {
        private readonly MovieCharactersDbContext _context;
        public MovieService(MovieCharactersDbContext context)
        {
            _context = context;
        }

        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies.ToListAsync();
        }

        public async Task<IEnumerable<Character>> GetMovieCharactersAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                throw new KeyNotFoundException();
            }

            return await _context.Movies.Where(m => m.Id == id).SelectMany(m => m.Characters).ToListAsync();
        }

        public async Task<Movie> GetSpecificMovieAsync(int id)
        {
            return await _context.Movies.FindAsync(id);
        }

        public bool MovieExists(int id)
        {
            return _context.Movies.Any(movie => movie.Id == id);
        }

        public async Task UpdateCharactersInMovieAsync(int movieId, int[] characters)
        {
            Movie movieToUpdateCharacters = await _context.Movies
                .Include(c => c.Characters)
                .Where(m => m.Id == movieId)
                .FirstAsync();
            List<Character> chars = new List<Character>(movieToUpdateCharacters.Characters);

            //check if characters with ids specified in charactersIds exist in database
            foreach (int charId in characters)
            {
                Character character = await _context.Characters.FindAsync(charId);
                if (character == null)
                    throw new KeyNotFoundException();
                chars.Add(character);
            }
            movieToUpdateCharacters.Characters = chars;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
