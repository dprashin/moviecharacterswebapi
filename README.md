# MovieCharactersWebAPI

ASP.NET core web API project.
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
-->

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    <li><a href="#license">License</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
An Entity Framework Code First workflow and an ASP.Net Web API written in C#. Also, includes Migration folder for creating/generating database with seeded data. This project also contains swagger documentation explaining on how to consume backened services. Furthermore, DTO objects are used to encapsulate the domain objects and decouple the Entity framework and the controllers. 
<!-- LICENSE -->
## License

Distributed under the MIT License. See [MIT License](https://opensource.org/licenses/MIT) for more information.
